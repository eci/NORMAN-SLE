# To-Do Items for the NORMAN-SLE

The home for lists in the queue (to-do) lists

## Updates
- Exposome Exporer (v3.0 out, we have v2)
- PRORISKPFAS checking (PubChem CIDs)
- TIRECHEM (more inputs from Bert)

## TO-DOs

- UBAUSE (??) (UBA REACH plus Use information)
	- see email from Jaro 7/12/2021
- JRC Marine Contaminants
- KEMI PFAS in Cosmetics (on hold - too few structures)
- ECHA SVHCs?
	- https://echa.europa.eu/candidate-list-table
	-  https://echa.europa.eu/chemicals-in-our-life/which-chemicals-are-of-concern/svhc
	- https://echa.europa.eu/information-on-chemicals/candidate-list-substances-in-articles
	- https://echa.europa.eu/substances-of-very-high-concern-identification
	-  https://echa.europa.eu/substances-of-very-high-concern-identification-explained

