CHLORINETPS

Source : CHLORINE_TPs database is associated to the publication https://doi.org/10.1016/j.teac.2021.e00148 
C. Postigo, R. Gil-Solsona, M.F. Herrera-Batista, P. Gago-Ferrero, N. Alygizakis, L. Ahrens, K. Wiberg. "A
step forward in the detection of byproducts of anthropogenic organic micropollutants in chlorinated water.
trend in Environmetnal Analytical Chemistry. Volume 32, December 2021, e00148"


Number : S87
Code : CHLORINETPS
Short Description : List of chlorination byproducts of 137 CECs and small disinfection byproducts

Description: A list of chlorination byproducts of 137 contaminants of emerging concern (CECs) and small molecular weight
disinfection byproducts from the CHLORINE_TPs database, described in Postigo et al DOI: 10.1016/j.teac.2021.e00148. 91% are amenable to LC-ESI-HRMS. 

S87 | CHLORINETPS | List of chlorination byproducts of 137 CECs and small disinfection byproducts
