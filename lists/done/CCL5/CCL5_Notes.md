# Notes from CCL5 List (https://www.epa.gov/ccl/draft-ccl-5-chemicals)

- Note 1: Chemical Abstracts Service Registry Number (CASRN) is a unique identifier assigned 
by the Chemical Abstracts Service (a division of the American Chemical Society) to every 
chemical substance (organic and inorganic compounds, polymers, elements, nuclear particles, 
etc.) in the open scientific literature. It contains up to 10 digits, separated by hyphens 
into three parts.

- Note 2: �Distributed Structure Searchable Toxicity Substance Identifiers (DTXSID) 
is a unique substance identifier used in EPA�s CompTox Chemicals database, where a 
substance can be any single chemical, mixture or polymer.


- Note 3: Toxins naturally produced and released by some species of cyanobacteria 
(previously known as "blue-green algae"). The group of cyanotoxins includes, but 
is not limited to: anatoxin-a, cylindrospermopsin, microcystins, and saxitoxin.

- Note 4: This group includes 23 unregulated DBPs as shown in the table:�Draft CCL 
5 Chemical Disinfection Byproducts Group.

- Note 5: Refer to list S85 MICROCYSTINS: https://doi.org/10.5281/zenodo.5665355

- Note 6: This group is inclusive of any PFAS (except for PFOA and PFOS). For the 
purposes of this document, the structural definition of PFAS includes per- and 
polyfluorinated substances that structurally contain the unit R-(CF2)-C(F)(R')R''. 
Both the CF2 and CF moieties are saturated carbons and none of the R groups 
(R, R' or R'') can be hydrogen.
