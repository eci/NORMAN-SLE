Column header legends
* 	OECD (Information on individual structure categories used in the spreadsheets and supplementary information) https://www.oecd.org/officialdocuments/publicdisplaydocumentpdf/?cote=ENV/CBC/MONO%282021%2925&docLanguage=en
* * 	US DSSTox (DTXSID records – List definition as of March 2018). https://www.epa.gov/chemical-research/distributed-structure-searchable-toxicity-dsstox-database
***	PFASMASTER (Last Updated: August 10th 2021) https://comptox.epa.gov/dashboard/chemical-lists/pfasmaster
****	 W WELLINGTON; E EHRENSTOFER; C CIL; CH CHIRON; NC NOT COMMERCIAL;T TRC; M Merck-Sigma Aldrich; X ALL
