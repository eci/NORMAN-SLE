S111_PMTPFAS

source : 

Number : S111
Code : PMTPFAS
Short Name : Fluorine-containing Compounds in PMT Suspect Lists 


Short Description : List of fluorine-containing compounds extracted from existing suspect lists for PMT (persistent, mobile, toxic) compounds, 
currently S36 UBAPMT, S82 EAWAGPMT and S84 UFZHSFPMT.


Description : PMTPFAS is a list of fluorine-containing compounds extracted from existing suspect lists for PMT (persistent, mobile, toxic) compounds, 
currently S36 UBAPMT, S82 EAWAGPMT and S84 UFZHSFPMT. 
All entries contain fluorine but are not necessarily PFAS. Two salt entries were replaced with the F-containing parts only.

S111 | PMTPFAS |Fluorine-containing Compounds in PMT Suspect Lists

