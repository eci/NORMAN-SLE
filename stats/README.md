# Summary statistics for the NORMAN-SLE

A summary of statistics over all lists from 
[https://www.norman-network.com/nds/SLE/](https://www.norman-network.com/nds/SLE/) 
and
[https://zenodo.org/communities/norman-sle](https://zenodo.org/communities/norman-sle).
Per list breakdowns are provided in the files under "More details".

## NORMAN-SLE Summary

| Category | Number | Comment |
|----------|-----------|---------|
|Number of Lists | 119 | S0 to S118 |
|Total Unique Compounds | 127,264 | From [PubChem NORMAN-SLE](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=101) Tree |
|Total Live Substances | 130,828 | From [PubChem NORMAN-SLE Source](https://pubchem.ncbi.nlm.nih.gov/source/23819) Page |
|Total Live Annotations | 27,440 | From [PubChem NORMAN-SLE Source](https://pubchem.ncbi.nlm.nih.gov/source/23819) Page |
|Largest List | 120,514 | S0 NORMAN-SusDat |
|Total Views | 143,734 | From [Zenodo](https://zenodo.org/communities/norman-sle) (see below) |
|Total Downloads | 113,615 | From [Zenodo](https://zenodo.org/communities/norman-sle) (see below) |
|Citations | 230 | From [Zenodo](https://zenodo.org/communities/norman-sle) (see below) |

Last updated 16/04/2024.


## Zenodo Statistics Summary

| Category |16/04/2024|04/04/2023| 28/04/2022 |
|----------|----------|----------| ----------|
|Total Views | 143,734 | 86,005| 47,570 |
|Total Downloads | 113,615 | 97,197 | 68,569 |
|Citations | 230 | 61 | 40 |

More details in 
[NORMAN-SLE_Zenodo_stats_20220428.csv](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/blob/master/stats/NORMAN-SLE_Zenodo_stats_20220428.csv),
[NORMAN-SLE_Zenodo_stats_20230404.csv](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/blob/master/stats/NORMAN-SLE_Zenodo_stats_20230404.csv) and
[NORMAN-SLE_Zenodo_stats_20240416.csv](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/blob/master/stats/NORMAN-SLE_Zenodo_stats_20240416.csv). 
Note that the unique view/download statistics were discontinued on Zenodo in Oct. 2023.


## Most Viewed / Downloaded

Top 10 viewed/downloaded lists are given in the table below, 
with the data shown per list in Figure 1. 

| List | Code | Total Views | Total Downloads | Citations |
|------|------|--------------|------------------|-----------|
| S13 | [EUCOSMETICS](https://doi.org/10.5281/zenodo.2624118) | 21,254 | 14,835 | 6 |
| S73 | [METXBIODB](https://doi.org/10.5281/zenodo.4056560) | 8832 | 2573 | 4 |
| S00 | [SUSDAT](https://doi.org/10.5281/zenodo.10510477) | 5733 | 3692 | 19 |
| S60 | [SWISSPEST](https://doi.org/10.5281/zenodo.3544759) | 5384 | 4391 | 4 |
| S72 | [NTUPHTW](https://doi.org/10.5281/zenodo.3955664) | 4936 | 3747 | 2 |
| S75 | [CYANOMETDB](https://doi.org/10.5281/zenodo.4551528) | 4432 | 3327 | 3 |
| S50 | [CCSCOMPEND](https://doi.org/10.5281/zenodo.2658162) | 4363 | 1433 | 1 |
| S29 | [PHYTOTOXINS](https://doi.org/10.5281/zenodo.2652993) | 4105 | 1079 | 2 |
| S25 | [OECDPFAS](https://doi.org/10.5281/zenodo.2648775) | 3860 | 2479 | 9 |
| S66 | [EAWAGTPS](https://doi.org/10.5281/zenodo.3754448) | 3337 | 2784 | 6 |


Table based on total downloads/views from Zenodo, 16/04/2024. Figure based on data from 16/04/2024, 04/04/2023 and 28/04/2022.

<img src="misc/fig/SLE_downloads_views_2022vs2023vs2024.png">

_Figure 1: NORMAN-SLE Total Downloads / Views per List (Apr. 2022 vs 2023 vs 2024)._


## Zenodo Citation Summary 

| Category | 04/04/2023 | 01/05/2022 |
|----------|------------|------------|
|Total Citations | 61 | 40 |
|Total Lists Cited | 34 | 24 |
|Total Citing Articles | - | 19 |
|Internal Citing Articles | - | 12 |
|External Citing Articles | - | 7 |

More details in 
[NORMAN-SLE_Zenodo_Citations_20220501.csv](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/blob/master/stats/NORMAN-SLE_Zenodo_Citations_20220501.csv) 
and 
[NORMAN-SLE_Zenodo_Citations_DOIs.csv](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/blob/master/stats/NORMAN-SLE_Zenodo_Citations_DOIs.csv).
NB: A citation is considered "internal" if it involves co-authors who have contributed directly to the NORMAN-SLE.

