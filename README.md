# Welcome to the ECI NORMAN-SLE Code Repository!

The project collects ECI code, files and documentation related to the 
NORMAN Suspect List Exchange ([NORMAN-SLE](https://www.norman-network.com/nds/SLE/)).

The published lists are supported by files on the 
[NORMAN-SLE Zenodo](https://zenodo.org/communities/norman-sle)
Community. 

Main ECI players: Emma Schymanski & Hiba Mohammed Taha

## Contents of the ECI NORMAN-SLE Repo

| Folder | Description |
| ------ | ------ |
| [docs](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/tree/master/docs) | Documentation |
| [lists](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/tree/master/lists) | Files related to SLE lists |
| [stats](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/tree/master/stats) | Statistics related to SLE lists |
| [web](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/tree/master/web) | Web updates for EI to publish |


Files related to the lists that are work in progress, along 
with archives etc can be found on the ECI server under 
"DATA/NORMAN-SLE"

Note: this is currently a work in progress!
