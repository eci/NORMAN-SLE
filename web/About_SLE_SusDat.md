# Information about NORMAN SusDat and NORMAN-SLE

This document contains the "about" statements (previously disclaimer statements) for the NORMAN-SLE and NORMAN SusDat websites, as well as the header on the NORMAN-SLE website

## NORMAN-SLE Header: 
The NORMAN Suspect List Exchange ([NORMAN-SLE](https://www.norman-network.com/nds/SLE/)) was established in 2015 as a central access point for NORMAN members (and others) to find suspect lists relevant for their environmental monitoring questions. The NORMAN-SLE documents all individual collections that form a part of the merged collection [NORMAN SusDat](http://www.norman-network.com/nds/susdat/). The original SLE lists should be consulted to verify SusDat information if necessary (see Source column in SusDat). NORMAN-SLE versions are tracked on [Zenodo](https://zenodo.org/communities/norman-sle). 

Comments and contributions are welcome - please email us at [suspects@normandata.eu](mailto:suspects@normandata.eu).

Please refer to our [documentation](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/tree/master/docs) pages for: [citation](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/blob/master/docs/CitingSLE.md) instructions, [credits](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/blob/master/docs/CreditsSLE.md), [updates](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/blob/master/docs/UpdatesSLE.md), [license](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/blob/master/docs/LicenseSLE.md) details, [SDFs](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/blob/master/docs/SLEtoSDF.md) and other useful tips!


## About NORMAN SusDat: 
NORMAN SusDat is a "living database" compiling information provided by NORMAN network members and external contributors via the NORMAN Suspect List Exchange (NORMAN-SLE). NORMAN SusDat merges the many chemical lists on the SLE into a common format and includes all data suitable for screening purposes, along with selected identifiers and predicted values as a service for NORMAN members and beyond. SusDat is undergoing constant development and improvement to expand the coverage, together with contributors and cheminformatics experts. The original lists on the NORMAN-SLE (https://www.norman-network.com/nds/SLE/) should be consulted to verify chemical information if necessary (see Source column). Comments and contributions are welcome - please email us at suspects@normandata.eu.


## About NORMAN SLE:
The NORMAN Suspect List Exchange (NORMAN-SLE) is a collection of original chemical (suspect) lists provided by NORMAN network members and external contributors, which are merged together to form NORMAN SusDat (https://www.norman-network.com/nds/susdat/). The original lists should be consulted to verify SusDat information if necessary (see Source column); SLE versions are tracked on Zenodo (https://zenodo.org/communities/norman-sle). Comments and contributions are welcome - please email us at suspects@normandata.eu.



## Previous Disclaimer Statements

### SLE: 
DISCLAIMER: NORMAN SLE and NORMAN SusDat include information provided by NORMAN network members and external contributors, and are "living documents" undergoing constant curation and improvement. The original lists on the Suspect List Exchange should be consulted to verify chemical information if necessary (see Source column); versions are tracked on Zenodo. Please check back regularly and report any issues to suspects@normandata.eu

### SusDat:
DISCLAIMER: SusDat is a compilation of information provided by NORMAN network members and it is a "living document" undergoing constant curation and improvement. NORMAN SusDat has merged many chemical lists provided to the NORMAN Suspect Exchange into a common format and includes selected identifiers and predicted values as a service for NORMAN members. We are working with contributors and cheminformatics experts to resolve many issues and update the list accordingly. The original lists on the Suspect Exchange (https://www.norman-network.com/?q=suspect-list-exchange) should be consulted to verify chemical information if necessary (see Source column). Please report any issues to suspects@normandata.eu.