# Finding MS(/MS) Information for NORMAN-SLE lists via PubChem

This document describes how to use 
[PubChem](https://pubchem.ncbi.nlm.nih.gov/)
functionality to explore how many of the
[NORMAN-SLE](https://www.norman-network.com/nds/SLE/) compounds
have been measured in various mass spectrometry (MS) 
methods.

## Step 1: NORMAN-SLE in PubChem Classification Browser
To start, visit the NORMAN Suspect List Exchange in the 
PubChem Classification Browser (see _Figure 1_): 

https://pubchem.ncbi.nlm.nih.gov/classification/#hid=101


<img src="fig/PubChem_Classification_SLE_view_20220702.png" width="70%" height="70%">

_Figure 1: Screenshot of the NORMAN-SLE Classification Browser (2 July 2022)_


The rest of this documentation will describe how to find out how many of the 
NORMAN-SLE compounds have been measured by any form of mass spectrometry method 
(at least, from among all mass spectrometry data integrated in PubChem). While
this document will explain it for the entire NORMAN-SLE collection, please 
note that the Entrez search to start this can be performed on any list 
present in the NORMAN-SLE Classification Browser in PubChem (or any other area). 


## Step 2: Send NORMAN-SLE (or subset) to Entrez History
Pick the section of the NORMAN-SLE that you wish to explore (e.g., a given list), 
or send the entire NORMAN-SLE to the Entrez History by clicking on the 
blue number next to the top node of the tree (i.e., the first line, 
which contains the number 115,260 in _Figure 1_). 
This will open a PubChem Search window, shown in _Figure 2_. 

<img src="fig/PubChemSearch_SLE.png" width="70%" height="70%">

_Figure 2: Screenshot of the NORMAN-SLE in the PubChem Search interface (2 July 2022)_


Send this to Entrez by clicking the option circled in red to the right. 
Make sure you note the number of compounds (circled in red to the left), 
so that you can find the full query in the Entrez history later.
Note: for large queries such as this, several composite queries can be performed 
and will appear in the history as well.  

A new window will open, shown in _Figure 3_. Note the appearence 
of the composite query - and the final total matching the number from before. 
You do not need to do anything in this window - instead proceed to Step 3 below!

<img src="fig/SLEentrezQuery.png" width="70%" height="70%">

_Figure 3: View of the NORMAN-SLE search query in Entrez (2 July 2022)_


## Step 3: Explore NORMAN-SLE on the PubChem Table of Contents Tree

To search how many of the [NORMAN-SLE](https://www.norman-network.com/nds/SLE/)
compounds have been measured using any form of mass spectrometry (where the data 
is present in PubChem), the next stop is the PubChem Table of Contents (TOC) Tree.
The PubChem TOC Tree is at https://pubchem.ncbi.nlm.nih.gov/classification/#hid=72.
If you already had the link open, please refresh (F5, or browser refresh button) 
after performing the Entrez query. 

Next, filter the [PubChem TOC Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=72)
contents by the [NORMAN-SLE](https://www.norman-network.com/nds/SLE/) contents
by selecting the correct Entrez history, shown in _Figure 4_ below. 

<img src="fig/SLEinTOC_wInset.png" width="70%" height="70%">

_Figure 4: Filter the PubChem TOC Tree by NORMAN-SLE content. Inset: an extended view of the TOC contents (2 July 2022)_

As shown in _Figure 4_, the contents are now filtered by the content of the 
NORMAN-SLE. Scroll down to find the "Spectral Information" section, shown with 
the purple outline in _Figure 4_. This is where the mass spectrometry information appears. 
The "Information Sources" category is also outlined, this is useful to determine 
in which mass spectral library the information may be contained. 

_Figure 5_ (left image) shows how many NORMAN-SLE entries have mass spectrometry 
information in PubChem. Over 33,000 SLE chemicals have been measured via 
mass spectrometry, >29,500 via gas chromatography mass spectrometry (GC-MS) 
and several thousand by liquid chromatography (LC-MS). Note that the 
MS-MS and "Other MS" sections also contain majority soft ionisation 
compounds that are likely LC-amenable. 

The image on the right of _Figure 5_ shows a subset of the information sources. 
Over 7,000 entries come from [MassBank EU](https://massbank.eu/MassBank/), >10,000 
entries from [MoNA](https://mona.fiehnlab.ucdavis.edu/) 
and >24,000 entries from the [NIST](https://chemdata.nist.gov/) library. 

<img src="fig/MSinSLEandTOC_byInfoSource.png" width="70%" height="70%">

_Figure 5: (left) NORMAN-SLE compounds with Mass Spectrometry annotation. (right) Information sources, highlighting the number of compounds from MassBank EU, MoNA and NIST (2 July 2022)_

Note that it is possible to do more powerful searches or combinations of searches 
to interrogate the mass spectrometry data further via the Entrez "Advanced Search" options, 
or via building queries in a manner similar to that described in this document. 
For further details about Entrez and PubChem capabilities, please refer to 
the PubChem documentation: https://pubchemdocs.ncbi.nlm.nih.gov/advanced-search-entrez. 
A contact email address for further queries is also given below. 


## Step 4: Browse Individual Records 

The mass spectrometry data available in PubChem includes summary experimental details, Top X 
peak summaries (even for the NIST data), images, source information and references. 
Find out more by browsing the individual compound pages. To find out which compounds 
these are, again, click on the blue numbers besides the category of interest 
(or the total number) in the Classification Browser (e.g., _Figure 1, 4 or 5_), 
and open a new PubChem Search window.
As shown in _Figure 6_, this will let you browse the entries 
individually (click on the hyperlinked CID), or download the entire set of compounds. 
Note that the MS data is not currently present in the download file.

<img src="fig/PubChemSearch_MSMS.png" width="70%" height="70%">

_Figure 6: Search window to browse or download NORMAN-SLE entries with MS data (2 July 2022)_


If you sort by "Create Date" (see _Figure 6_), then the most recent 
additions to PubChem appear. As shown in _Figure 7_, this is 
currently from a dataset added as part of the NORMAN-SLE. The 
S74 REFTPS dataset is designed to add literature data into 
open resources to fill data gaps - including both the structures 
and the associated data (in this case, MS/MS information and 
also transformation reactions, not shown). 

<img src="fig/MSMS_82FTUnsatAldGlu.png" width="70%" height="70%">

_Figure 7: Experimental MS/MS values for CID 162393340, showing data from S74 REFTPS (2 July 2022)_



For compounds with experimental MS data available in PubChem, 
these can be viewed individually in the compound record, as shown above. 
Once on a compound record, use the menu to browse to the 
"Spectral Information" section and then "Mass Spectrometry" 
(see _Figure 7_, right panel). 
Alternatively, the URL to access this section can be constructed 
easily once the PubChem Compound Identifier (CID) is known, e.g., 
from the download file. The URL follows the pattern 
https://pubchem.ncbi.nlm.nih.gov/compound/162393340#section=Mass-Spectrometry
where 162393340 is the CID and the text after the hash navigates 
directly to the Mass Spectrometry section. 
_Figure 7_ shows the example from the URL above, with the 
navigation panel shown on the right of the figure. 




## Step 5: Obtain all MS data

It is possible to access all data displayed in PubChem programatically. 
The harmonization of the mass spectrometry data to enable this in a 
useful manner is currently work in progress. 
Example code snippets will be posted once this is possible, and the data will be 
integrated into NORMAN SusDat. 
In the interim, please contact the 
[ECI NORMAN-SLE team](mailto:normansle@uni.lu) or 
[PubChem help mailing list](mailto:pubchem-help@ncbi.nlm.nih.gov)
for more information. 


## Enjoy!

Document written 2 July 2022 by Emma Schymanski, UniLu. 
