# NORMAN-SLE Credits: Coordinators & Contributors

NORMAN-SLE exists primarily due to voluntary and in kind contributions. 
Please give these efforts appropriate attribution by following our 
[citation guidelines](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/blob/master/docs/CitingSLE.md).

## Coordinators

| Role | Names | Affiliation | 
| ------ | ------ | ------ |
| Coordination | Emma Schymanski | LCSB, University of Luxmebourg |
| Support | Hiba Mohammed Taha | LCSB, University of Luxembourg |
| Support | Jolly Komolo | LCSB, University of Luxembourg |
| Webmaster | Natalia Glowacka | Environmental Institute, Slovakia |
| NDS Host | Jaroslav Slobodnik | Environmental Institute, Slovakia |
| IT | Lubos Cirka | Environmental Institute, Slovakia |
| Integration | Nikiforos Alygizakis | Environmental Institute, Slovakia |
| Predicted Values | Reza Aalizadeh | University of Athens |
| Predicted Values | Nikos Thomaidis | University of Athens |
| CompTox Integration | Antony Williams | US EPA |
| PubChem Integration | Evan Bolton | NCBI/NLM/NIH |
| PubChem Integration | Jian (Jeff) Zhang | NCBI/NLM/NIH |
| PubChem Integration | Paul Thiessen | NCBI/NLM/NIH |


## Contributors

Please refer to the 
[NORMAN-SLE](https://www.norman-network.com/nds/SLE/) 
website for an overview of all contributors!

## Contact

Contact details are on the 
[NORMAN-SLE](https://www.norman-network.com/nds/SLE/) 
website or by writing to suspects _at_ normandata _dot_ eu 
(reaches EI & LCSB representatives). 
