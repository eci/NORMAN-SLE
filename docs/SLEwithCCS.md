# Finding CCS Values for NORMAN-SLE lists via PubChem

This document describes how to use 
[PubChem](https://pubchem.ncbi.nlm.nih.gov/)
functionality to explore how many collision cross section (CCS) 
values are available for compounds in 
[NORMAN-SLE](https://www.norman-network.com/nds/SLE/) lists.

## Step 1: NORMAN-SLE in PubChem Classification Browser
To start, visit the NORMAN Suspect List Exchange in the 
PubChem Classification Browser (see _Figure 1_): 

https://pubchem.ncbi.nlm.nih.gov/classification/#hid=101


<img src="fig/PubChem_Classification_SLE_view_20220702.png" width="70%" height="70%">

_Figure 1: Screenshot of the NORMAN-SLE Classification Browser (2 July 2022)_


## Step 2: Browse Compounds with CCS Values in the Aggregated CCS Tree on PubChem
Next, visit the 
[Aggregated CCS Classification](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=106)
Tree in PubChem (see _Figure 2_):

<img src="fig/AggregatedCCStree.png" width="70%" height="70%">

_Figure 2: Screenshot of the Aggregated CCS Classification Browser (2 July 2022)_


## Step 3: Browse NORMAN-SLE Tree with existing CCS Values

To search how many of these are in the [NORMAN-SLE](https://www.norman-network.com/nds/SLE/), 
click on the blue number of compounds in a given category (see yellow highlighting 
in _Figure 2_). This will open up the PubChem Search interface, shown in 
_Figure 3_ below. 


<img src="fig/AggregatedCCSsearch.png" width="70%" height="70%">

_Figure 3: Search for the entire contents of the Aggregated CCS Tree (2 July 2022)_

Select the "Push to Entrez" option circled in red. This will bring up a new 
window again, shown in _Figure 4_. You do not need to do anything in this 
window - instead return to the NORMAN-SLE classification browser 
and proceed as described below!

<img src="fig/AggregatedCCSentrez.png" width="70%" height="70%">

_Figure 4: View of search query in Entrez (2 July 2022)_

Return to the NORMAN-SLE Classification browser 
(https://pubchem.ncbi.nlm.nih.gov/classification/#hid=101)
and ***refresh*** the window. You may have to be a little stubborn
(F5, or use the refresh button). A new menu item "Filter by Entrez History"
will appear, shown in _Figure 5_: 

<img src="fig/FilterSLEbyCCS.png" width="70%" height="70%">

_Figure 5: Filter the NORMAN-SLE content by existing CCS Values (2 July 2022)_

This shows, for instance, that on 2 June 2022, there were experimental 
CCS values for 3,198 compounds in the NORMAN-SLE. The numbers beside 
the individual lists show how many of the entries in that list have 
experimental CCS values in PubChem. For example, there are 18 entries 
in S47 ECHAPLASTICS with experimental CCS values, or 267 entries in 
S13 EUCOSMETICS with experimental CCS values. 

## Step 4: Browse Individual Records or Download ...

To find out which compounds these are, again, click on the blue 
numbers besides the category of interest (or the total number, as
shown in _Figure 5_), and open a new PubChem Search window.
As shown in _Figure 6_, this will let you browse the entries 
individually, or download the entire set of compounds (note that 
the CCS values are not currently present in the download file).

<img src="fig/SearchFilteredSLE_CCS.png" width="70%" height="70%">

_Figure 6: Search window to browse or download NORMAN-SLE entries with CCS values (2 July 2022)_


For compounds with experimental CCS values available in PubChem, 
these can be viewed individually in the compound record. 
Once on a compound record, use the menu to browse to the 
"Chemical and Physical Properties" section and select 
"Collision Cross Section" to navigate straight to these numbers
(see _Figure 7_, right panel). 
Alternatively, the URL to access this section can be constructed 
easily once the PubChem Compound Identifier (CID) is known, e.g., 
from the download file. The URL follows the pattern 
https://pubchem.ncbi.nlm.nih.gov/compound/1615#section=Collision-Cross-Section
where 1615 is the CID and the text after the hash navigates 
directly to the experimental CCS values. 
_Figure 7_ shows the example from the URL above, with the 
navigation panel shown on the right of the figure. 

<img src="fig/CCSannotationExample.png" width="70%" height="70%">

_Figure 7: Experimental CCS values for CID 1615, here from CCSbase and S61 UJICCSLIB (2 July 2022)_




## Step 5: Obtain all CCS values

It is possible to access all CCS values programatically. 
Example code to do so is available at the 
[ECI GitLab pages](https://gitlab.lcsb.uni.lu/eci/pubchem/-/blob/master/annotations/CCS/CCS_retrieval/)
as [RMarkdown](https://gitlab.lcsb.uni.lu/eci/pubchem/-/blob/master/annotations/CCS/CCS_retrieval/RetrievingCCS.Rmd) 
or [PDF](https://gitlab.lcsb.uni.lu/eci/pubchem/-/raw/master/annotations/CCS/CCS_retrieval/RetrievingCCS.pdf?inline=false).
Summary tables of all experimental CCS values in PubChem are available 
on [GitLab](https://gitlab.lcsb.uni.lu/eci/pubchem/-/blob/master/annotations/CCS/CCS_retrieval/) 
or Zenodo [DOI:10.5281/zenodo.6800138](https://doi.org/10.5281/zenodo.6800138).

If you have any questions about this, please contact the 
[ECI NORMAN-SLE team](mailto:normansle@uni.lu) or 
[PubChem help mailing list](mailto:pubchem-help@ncbi.nlm.nih.gov)
for more information. 


## Enjoy!

Document written 2 July 2022 by Emma Schymanski, UniLu. 
Last updated: 5 July 2022. 
