# NORMAN-SLE License Details

All NORMAN-SLE collections are shared under a 
Creative Commons Attribution 4.0 International License 
([CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)) 
unless otherwise stated. 

NORMAN-SLE exists primarily due to voluntary and in kind contributions. 
Please give these efforts appropriate attribution by following our 
[citation guidelines](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/blob/master/docs/CitingSLE.md).
