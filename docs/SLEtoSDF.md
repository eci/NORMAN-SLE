# Converting NORMAN-SLE lists to SDF via PubChem

This document details how to download NORMAN-SLE lists in 
alternative formats (other than those offered on the NORMAN-SLE 
[website](https://www.norman-network.com/nds/SLE/) or 
[Zenodo](https://zenodo.org/communities/norman-sle)) via 
[PubChem](https://pubchem.ncbi.nlm.nih.gov/).

## Step 1: NORMAN-SLE in PubChem Classification Browser
To start, visit the NORMAN Suspect List Exchange in the 
PubChem Classification Browser (see _Figure 1_): 

https://pubchem.ncbi.nlm.nih.gov/classification/#hid=101


<img src="fig/PubChem_Classification_SLE_view.png" width="50%" height="50%">

_Figure 1: Screenshot of the NORMAN-SLE Classification Browser_

The lists at the top are the subset of the SLE lists with specific 
classification content, 
followed by a listing of all lists from S0 to the latest entry
(see _Figure 2_). 

<img src="fig/PubChem_Classification_SLE_list_view.png" width="70%" height="70%">

_Figure 2: Screenshot of Lists within NORMAN-SLE Classification Browser_

## Step 2: Choose the list to download
To download a list, click on the blue number of compounds in 
the Classification Browser view. In the example below, S25 OECDPFAS
will be downloaded - see yellow highlight in _Figure 3_:

<img src="fig/Download_S25_OECDPFAS.png" width="70%" height="70%">

_Figure 3: Screenshot of downloading S25 OECDPFAS_

This will then open up the PubChem Search interface, shown 
in the next screenshot, where a download button is available
(highlighted in yellow; _Figure 4_):

<img src="fig/S25_OECDPFAS_PubChem_Search.png" width="50%" height="50%">

_Figure 4: Screenshot of S25 OECDPFAS in PubChem Search Interface_

## Step 3: Choose the download formats

Once in the PubChem Search interface, click on the download 
option to expand the window and see the choices. At the top 
a CSV export is available, whereas the SDF format needed 
for instance for Compound Discoverer is in the second row. 
Both are circled in red in _Figure 5_. 

<img src="fig/Download_Formats.png" width="50%" height="50%">

_Figure 5: Screenshot of Download Formats_

Click on the respective button to start the download ... 
and very soon the SDF file is ready for further use!

See _Figure 6_ for a preview.

<img src="fig/S25_OECDPFAS_SDF.png" width="50%" height="50%">

_Figure 6: Screenshot of the downloaded SDF for S25 OECDPFAS_


## Step 4: Enjoy!





