# A collection of documentation for the NORMAN-SLE

This [docs](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/tree/master/docs) 
subfolder contains useful documentation and tips to use the lists 
on the NORMAN Suspect List Exchange ([NORMAN-SLE](https://www.norman-network.com/nds/SLE/)).
The contents of this folder will grow progressively as we add tips and tricks and ideas. 

Do you have an idea? Please email the [ECI NORMAN-SLE team](mailto:normansle@uni.lu)!

## Documentation Menu / Overview

| File | Description |
| ------ | ------ |
| [CitingSLE](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/blob/master/docs/CitingSLE.md) | Citing NORMAN-SLE Lists |
| [Credits](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/blob/master/docs/CreditsSLE.md) | NORMAN-SLE Credits: Coordinators & Contributors |
| [License](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/blob/master/docs/LicenseSLE.md) | License Details for NORMAN-SLE Lists |
| [SLEtoSDF](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/blob/master/docs/SLEtoSDF.md) | Converting NORMAN-SLE lists to SDF via PubChem |
| [SLEwithCCS](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/blob/master/docs/SLEwithCCS.md) | Finding CCS Values for NORMAN-SLE lists via PubChem |
| [SLEwithMS](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/blob/master/docs/SLEwithMS.md) | Finding MS data for NORMAN-SLE lists via PubChem |
| [Updates](https://gitlab.lcsb.uni.lu/eci/NORMAN-SLE/-/blob/master/docs/UpdatesSLE.md) | Updates for the NORMAN-SLE |


