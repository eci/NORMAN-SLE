# Citing NORMAN-SLE Lists

The following sections describe how to cite the NORMAN-SLE 
in various scenarios.

## Citing the NORMAN-SLE Website

Please cite the full website as: 

NORMAN Network (YYYY). NORMAN Suspect List Exchange (NORMAN-SLE) Website. https://www.norman-network.com/nds/SLE/. Accessed DD-MMM-YYYY.

## Citing Individual NORMAN-SLE Lists

Please cite individual collections using the 
[Zenodo](https://zenodo.org/communities/norman-sle) dataset DOI 
as well as additional references given per list – this helps us 
track the contribution of our efforts and acknowledge the contributing authors! 

You can find this information on the 
[NORMAN-SLE](https://www.norman-network.com/nds/SLE/) website
as shown in Figure 1:

<img src="fig/SLE_refs.png" width="80%" height="80%">

_Figure 1: Finding reference information on the NORMAN-SLE webpage (red boxes). Blue boxes outline the Zenodo DOI for that list._

You can find the detailed “Cite as” instructions on the Zenodo 
pages for the given list, shown in Figure 2. Navigate to the 
Zenodo record by clicking on the DOI indicated in the blue 
boxes in Figure 1 above. 

<img src="fig/Zenodo_CiteAs.png" width="40%" height="40%">

_Figure 2: The "Cite as" instructions in Zenodo, for S0 SusDat._

Example: “NORMAN Network, Aalizadeh, R., Alygizakis, N., Schymanski, E., Slobodnik, J., Fischer, S., & Cirka, L. (2021). S0 | SUSDAT | Merged NORMAN Suspect List: SusDat (Version NORMAN-SLE-S0.0.3.2). _Data set_. Zenodo. DOI: 10.5281/zenodo.4558070.“

To cite all versions of a list, use the DOI from the 
"Cite all versions" box (Figure 3). 

<img src="fig/Zenodo_CiteAll.png" width="40%" height="40%">

_Figure 3: The "Cite all versions" instructions in Zenodo, for S0 SusDat._

Example for all versions: “NORMAN Network, Aalizadeh, R., Alygizakis, N., Schymanski, E., Slobodnik, J., Fischer, S., & Cirka, L. (YYYY). S0 | SUSDAT | Merged NORMAN Suspect List: SusDat. _Data set_. Zenodo. DOI: 10.5281/zenodo.2664077.“


## Citing NORMAN-SLE Collections in PubChem

The NORMAN-SLE is on PubChem via the 
[Classification Browser](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=101) 
and as a 
[Data Source](https://pubchem.ncbi.nlm.nih.gov/source/23819) 
and can be cited as following: 

NORMAN Network & NCBI/NLM/NIH (YYYY). NORMAN Suspect List 
Exchange Classification Browser on PubChem. https://pubchem.ncbi.nlm.nih.gov/classification/#hid=101. Accessed DD-MMM-YYYY.

NORMAN Network & NCBI/NLM/NIH (YYYY). NORMAN Suspect List Exchange Data 
Source in PubChem. https://pubchem.ncbi.nlm.nih.gov/source/23819. Accessed DD-MMM-YYYY.

Please also consider citing PubChem as described in the 
[PubChem Citation Guidelines](https://pubchemdocs.ncbi.nlm.nih.gov/citation-guidelines).


## Citing NORMAN-SLE Collections in CompTox

The NORMAN-SLE lists are also on the 
[Chemical Lists](https://comptox.epa.gov/dashboard/chemical-lists/) 
page at the CompTox Chemicals Dashboard. Individual URLs can
be found on the NORMAN-SLE website (see third column, Figure 1)
or on the CompTox webpage. Please cite the authors of the 
original list in addition to the US EPA.

Example: “NORMAN Network, Aalizadeh, R., Alygizakis, N., Schymanski, E., Slobodnik, J., Fischer, S., Cirka, L., & US EPA (YYYY). S0 | SUSDAT | Merged NORMAN Suspect List: SusDat on the CompTox Chemcials Dashboard. https://comptox.epa.gov/dashboard/chemical-lists/susdat. Accessed DD-MMM-YYYY.“

Please also consider citing CompTox ([details here](https://doi.org/10.1186/s13321-017-0247-6)).

